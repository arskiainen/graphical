# graphical

A vulkan thingy

Currently a simple model viewing application that loads gltf(maybe) and
renders using vulkan and winit.

## Ambitions
While I guess this could turn into a game engine toy, for the moment I'm
more concerned with just learning more Rust and using Vulkan to interface
with the GPU. Maybe some raytraced shadows with compute or the like. Before
all that, it is likely I will need to rewrite the renderer with gfx-hal or
Rendy or wgpu, since vulkano's future seems gloomy.

TODO:
- [ ] Config serde
- [ ] Input state
- [ ] World state
- [ ] Physics? maybe simple collision with boxes and spheres or physx-rs
- [ ] Font rendering might help with gui

