fn main() {
    println!("cargo:rerun-if-changed=shaders/shader.vert");
    println!("cargo:rerun-if-changed=shaders/shader.frag");
    println!("cargo:rerun-if-changed=shaders/shader.comp");
}