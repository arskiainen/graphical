//! A vulkan thingy
//! 
//! Currently a simple model viewing application that loads gltf(maybe) and 
//! renders using vulkan and winit.
//! 
//! # Ambitions
//! While I guess this could turn into a game engine toy, for the moment I'm 
//! more concerned with just learning more Rust and using Vulkan to interface 
//! with the GPU. Maybe some raytraced shadows with compute or the like. Before 
//! all that, it is likely I will need to rewrite the renderer with gfx-hal or
//! Rendy or wgpu, since vulkano's future seems gloomy.
//! 
//! TODO:
//! - [ ] Config serde
//! - [ ] Input state
//! - [ ] World state
//! - [ ] Physics? maybe simple collision with boxes and spheres or physx-rs
//! - [ ] Font rendering might help with gui
//! 

use std::time::{Duration, Instant};

use std::path::Path;
use winit::{EventsLoop, WindowEvent, Event, DeviceEvent, KeyboardInput, ElementState};

mod vulkan {
    pub mod vulkan_render;
    pub mod vulkan_compute;
}
mod three {
    pub mod octree;
    pub mod types;
    pub mod world;
}

use vulkan::vulkan_render;


fn main() -> Result<(), Box<dyn std::error::Error>> {
    let start = Instant::now();

    let mut event_loop = EventsLoop::new();
    let mut app = vulkan_render::VulkanRender::init(&event_loop);

    let path = Path::new("assets/non_trivial_box/non_trivial_box.gltf");
    app.setup_scene(path);

    println!("Loaded: {} millis", start.elapsed().as_millis());
    
    let mut frame_time = 0;
    let mut frame_count = 0;
    loop {
        let start = Instant::now();
        app.draw();
        frame_time += start.elapsed().as_micros();
        frame_count += 1;

        if frame_time > 1000000 {
            println!("Avg. Frametime: {} millis, FPS: {}", (frame_time / frame_count) as f32 / 1000., frame_count);
            frame_time = 0;
            frame_count = 0;
        }
        let mut done = false;
        event_loop.poll_events(|event| {
            match event {
                Event::WindowEvent { event: WindowEvent::CloseRequested, .. } => done = true,
                Event::WindowEvent { event: WindowEvent::Resized(_), .. } => app.recreate_swapchain(),
                Event::DeviceEvent { 
                    event: DeviceEvent::MouseMotion { delta }, .. } => {
                        // Quick and dirty camera control.
                        if delta != (0.0_f64, 0.0_f64) {
                            app.rotate_view(delta)
                        }
                    },
                Event::DeviceEvent { 
                    event: DeviceEvent::Key(KeyboardInput{scancode, state, ..}), .. } => {
                    println!("keyboard input: {:?} {:?}", scancode, state);
                    if scancode == 17 && state == ElementState::Released {
                        app.toggle_wireframe();
                    }
                },
                _ => ()
            }
        });

        if done { break; }
    }
    
    println!("Clean exit!");
    Ok(())
}
