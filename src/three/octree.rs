use cgmath::Vector3;
use cgmath::prelude::*;

use super::types::Entity;
use super::types::Ray;

pub struct Octree {
    entities: Option<Vec<Entity>>,
    octants: Option<Vec<Octree>>,
    center: Vector3<f64>,
    radius: f64,
    own_count: u32,
    sum_count: u32,
    child_max: u32,
}

impl Octree {
    pub fn new(center: Vector3<f64>, radius: f64, child_max: u32) -> Octree {
        Octree {
            entities: None,
            octants: None,
            center,
            radius,
            own_count: 0,
            sum_count: 0,
            child_max,
        }
    }

    pub fn new_from_entities(center: Vector3<f64>, radius: f64, child_max: u32, entities: Vec<Entity>) -> Octree {
        let mut tree = Octree::new(center, radius, child_max);
        for entity in entities {
            tree.insert(entity);
        }
        tree
    }

    pub fn insert(&mut self, entity: Entity) {
        if self.octants.is_some() {
            self.place_entity(entity);
            return;
        }

        if self.own_count > self.child_max {
            self.init_octants();
            let old_entities = self.entities.take();
            for old_entity in old_entities.unwrap() {
                self.place_entity(old_entity);
            }
            self.own_count = 0;
            self.place_entity(entity);
        } else {
            self.entities.as_mut().unwrap().push(entity);
            self.own_count += 1;
        }
    }

    pub fn remove(&mut self, entity: Entity) {
        if let Some(own_entities) = &mut self.entities {
            for (index, own_entity) in own_entities.iter().enumerate() {
                if own_entity.id == entity.id {
                    own_entities.remove(index);
                    self.own_count -= 1;
                    break;
                }
            }
        } else {
            let idx = oct_select(self.center, entity.pos);
            if let Some(children) = &mut self.octants {
                children[idx].remove(entity);
                self.sum_count -= 1;
            }
        }
    }

    fn place_entity(&mut self, entity: Entity) {
        let idx = oct_select(self.center, entity.pos);
        if let Some(children) = &mut self.octants {
            children[idx].insert(entity);
            self.sum_count += 1;
        }
    }

    pub fn relocate(&mut self, entity: Entity, new_pos: Vector3<f64>) {
        self.remove(entity);
        let mut entity = entity;
        entity.pos = new_pos;
        self.insert(entity);
    }

    pub fn query_pos(&self, pos: Vector3<f64>) -> Option<Vec<Entity>> {
        if !self.bounds_point(pos) {
            return None;
        }
        let mut neighbours = Vec::new();
        let idx = oct_select(self.center, pos);
        if let Some(children) = &self.octants {
            if let Some(entities) = children[idx].query_pos(pos) {
                neighbours = entities;
            };
        } else {
            if let Some(entities) = &self.entities {
                neighbours = entities.clone();
            };
        };
        Some(neighbours)
    }

    fn bounds_point(&self, pos: Vector3<f64>) -> bool {
        let checks = (pos - self.center).map(|offset| {
            offset < self.radius
        });
        checks.x && checks.y && checks.z
    }

    fn bounds_box(&self, pos: Vector3<f64>, radius: f64) -> bool {
        let checks = (pos - self.center).map(|offset| {
            offset < self.radius + radius
        });
        checks.x && checks.y && checks.z
    }

    fn bounds_ray(&self, ray: &mut Ray) -> bool {
        if self.bounds_point(ray.origin) {
            return true; // ray originated from inside
        }
        let t = self.center - ray.origin;

        let min = t.map(|offset| { offset - self.radius })
            .mul_element_wise(ray.inv_dir());

        let max = t.map(|offset| { offset + self.radius })
            .mul_element_wise(ray.inv_dir());

        let tmin = min.x.min(max.x);
        let tmax = min.x.max(max.x);
        
        let tmin = tmin.max(min.y.min(max.y));
        let tmax = tmax.min(min.y.max(max.y));

        let tmin = tmin.max(min.z.min(max.z));
        let tmax = tmax.min(min.z.max(max.z));

        tmax >= tmin
    }

    pub fn query_bb(&self, pos: Vector3<f64>, radius: f64) -> Option<Vec<Entity>> {
        if !self.bounds_box(pos, radius) {
            return None;
        }
        let mut neighbours = Vec::new();
        let idx = oct_select(self.center, pos);
        if let Some(children) = &self.octants {
            for child in children {
                if let Some(entities) = children[idx].query_bb(pos, radius) {
                    neighbours = entities;
                    break;
                };
            }
        } else {
            if let Some(entities) = &self.entities {
                neighbours = entities.clone();
            };
        };
        Some(neighbours)
    }

    fn init_octants(&mut self) {
        let cx = self.center.x;
        let cy = self.center.y;
        let cz = self.center.z;

        let oct = 8;
        let mut octants = Vec::with_capacity(oct);

        for idx in 0 .. oct {
            let radius = self.radius / 2.0;
            let offset = Vector3 {
                x: idx % 1,
                y: idx % 2,
                z: idx % 4,
            };
            let center = Vector3 {
                x: cx + radius,
                y: cy + radius,
                z: cz + radius,
            };
            octants[idx] = Octree::new(center, radius, self.child_max);
        }
    }
}

/// Choose the octant to act on
/// 
/// # Arguments
/// 
/// * `self_pos` - A 3d vector denoting a position in space
/// * `pos` - A 3d vector denoting a position in space
/// 
/// # Returns
/// 
/// * `usize` - The chosen octant index
pub fn oct_select(pos1: Vector3<f64>, pos2: Vector3<f64>) -> usize {
    let offset = pos2 - pos1;
    let x = (offset.x.signum() + 1.) * 0.5;  // 0 or 1
    let y = offset.y.signum() + 1.;         // 0 or 2
    let z = (offset.z.signum() + 1.) * 2.;  // 0 or 4

    // idx = usize from bitmask in an ugly way? zyx e.g. 011 = 3
    (x + y + z) as usize
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn oct_select_last() {
        let pos1 = Vector3 {
            x: 0.5,
            y: 45.75,
            z: 74.0,  
        };
        let pos2 = Vector3 {
            x: 0.75,
            y: 46.25,
            z: 81.54,
        };
        assert_eq!(oct_select(pos1,pos2), 7);
    }

    #[test]
    fn oct_select_first() {
        let pos1 = Vector3 {
            x: 0.5,
            y: 45.75,
            z: 74.0,  
        };
        let pos2 = Vector3 {
            x: -0.75,
            y: -46.25,
            z: -81.54,
        };
        assert_eq!(oct_select(pos1,pos2), 0);
    }
    
    #[test]
    fn oct_select_third() {
        let pos1 = Vector3 {
            x: 0.5,
            y: 0.75,
            z: 74.0,  
        };
        let pos2 = Vector3 {
            x: 0.75,
            y: 4.25,
            z: -1.54,
        };
        assert_eq!(oct_select(pos1,pos2), 3);
    }
}