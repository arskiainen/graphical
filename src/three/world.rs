use super::types::Entity;

pub struct World {
    entities: Vec<Entity>,
}