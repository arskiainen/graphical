use cgmath::Vector3;
use cgmath::Matrix4;

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Entity {
    pub id: u32,
    pub pos: Vector3<f64>,
    //pub components: Box<Components>,
}

#[derive(Default, Copy, Clone)]
pub struct Vertex {
    pub pos: [f32; 3],
    pub normal: [f32; 3],
    pub tex_coord: [f32;2],
}

impl Vertex {
    pub fn new(pos: [f32;3], normal: [f32;3], tex_coord: [f32; 2]) -> Vertex {
        Vertex {
            pos,
            normal,
            tex_coord
        }
    }
}

#[derive(Copy, Clone)]
pub struct UniformBufferObject {
    pub model: Matrix4<f32>,
    pub view: Matrix4<f32>,
    pub proj: Matrix4<f32>,
}

pub struct Ray {
    pub origin: Vector3<f64>,
    pub direction: Vector3<f64>,
    inv_dir: Option<Vector3<f64>>,
}

impl Ray {
    pub fn inv_dir(&mut self) -> Vector3<f64>{
        match self.inv_dir {
            Some(dir) => dir,
            None => {
                let inv_dir = self.direction.map(|component| {
                    1./component
                });
                self.inv_dir = Some(inv_dir);
                inv_dir
            },
        }
    }
}

pub struct Components {
    dynamic: Option<Dynamic>,
}

struct Dynamic {
    vel: Vector3<f64>,
    acc: Vector3<f64>,
    dyn_props: Box<DynProps>,
}

struct DynProps {
    collider: Option<Collider>,
}

enum Collider {
    Bounds,
    Sphere,
}
