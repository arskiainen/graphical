use std::sync::Arc;
use std::path::Path;

use vulkano::instance::{Instance, PhysicalDevice};
use vulkano::device::{Device, Queue};
use vulkano::buffer::{
    BufferUsage, 
    CpuAccessibleBuffer, 
    ImmutableBuffer,
    BufferAccess, 
    TypedBufferAccess
};
use vulkano::command_buffer::{
    AutoCommandBufferBuilder, 
    DynamicState
};
use vulkano::descriptor::descriptor_set::{
    PersistentDescriptorSet,
    DescriptorSet
};
use vulkano::image::{
    Dimensions, 
    SwapchainImage, 
    ImageUsage, 
    AttachmentImage, 
    ImmutableImage
};
use vulkano::sampler::{Sampler, SamplerAddressMode, Filter, MipmapMode};
use vulkano::pipeline::{
    GraphicsPipeline, 
    GraphicsPipelineAbstract, 
    viewport::Viewport
};
use vulkano::format::Format;
use vulkano::sync::GpuFuture;
use vulkano::framebuffer::{
    Framebuffer, 
    FramebufferAbstract, 
    Subpass, 
    RenderPassAbstract
};
use vulkano::swapchain::{
    Surface,
    Swapchain, 
    SurfaceTransform, 
    PresentMode
};

use vulkano_win::VkSurfaceBuild;

use winit::{EventsLoop, WindowBuilder, Window};

use cgmath::{Vector3, Matrix4, Rad, Deg, Point3}; // Maybe should use nalgebra instead.

use crate::three::types::{
    Vertex,
    UniformBufferObject,
};

vulkano::impl_vertex!(Vertex, pos, normal, tex_coord);

pub struct VulkanRender {
    surface: Arc<Surface<Window>>,
    dimensions: [u32;2],
    //physical_device_index: usize, // We might need this to e.g. check features after init
    device: Arc<Device>,
    graphics_queue: Arc<Queue>,
    //present_queue: Arc<Queue>, // Swapchain could use it's own queue for performance?
    render_pass: Arc<dyn RenderPassAbstract + Send + Sync>,
    graphics_pipeline: Arc<dyn GraphicsPipelineAbstract + Send + Sync>,

    swapchain: Arc<Swapchain<Window>>,
    swapchain_fb: Vec<Arc<dyn FramebufferAbstract + Send + Sync>>,
    depth_buffer: Arc<AttachmentImage>,

    vertex_buffers: Vec<Arc<dyn BufferAccess + Send + Sync>>,
    index_buffers: Vec<Arc<dyn TypedBufferAccess<Content=[u16]> + Send + Sync>>,

    // The uniform buffers should be split to have camera transforms global
    // and the object specific transforms index match the vertex buffers.
    // Some objects can have multiple meshes, so the structure might not be straight forward.
    uniform_buffers: Vec<Arc<CpuAccessibleBuffer<UniformBufferObject>>>,
    descriptor_sets: Vec<Arc<dyn DescriptorSet + Send + Sync>>,

    previous_frame_end: Option<Box<dyn GpuFuture>>,
    dynamic_state: DynamicState,
    recreate_swapchain: bool,

    camera: Point3<f32>, // Temporary for camera move hack
    rotation: (f32, f32), // Temporary for camera move hack
    wire: bool, // Wireframe toggle
}

impl VulkanRender {
    pub fn init(event_loop: &EventsLoop) -> VulkanRender {

        let instance = {
            // TODO: Figure out which extensions we are using/need beyond drawing to surface.
            let extensions = vulkano_win::required_extensions();
            Instance::new(None, &extensions, None)
                .expect("failed to create instance")
        };

        let physical = PhysicalDevice::enumerate(&instance).next()
            .expect("no device available");
        //let physical_device_index = physical.index();

        let queue_family = physical.queue_families()
            .find(|&q| q.supports_graphics())
            .expect("couldn't find a graphical queue family");

        let (device, mut queues) = {
            let device_ext = vulkano::device::DeviceExtensions {
                khr_swapchain: true,
                .. vulkano::device::DeviceExtensions::none()
            };

            Device::new(physical, physical.supported_features(), &device_ext, [(queue_family, 0.5)].iter().cloned())
                .expect("failed to create device")
        };

        let graphics_queue = queues.next().unwrap();

        let surface = WindowBuilder::new()
            .with_title("A window, whoop whoop!")
            .build_vk_surface(&event_loop, instance.clone())
            .unwrap();

        let mut dimensions = [1280, 400];
        let (swapchain, swap_images) =  {
            let caps = surface.capabilities(physical)
                .expect("failed to get surface capabilities");
            println!("Caps: {:?}", caps);
            dimensions = caps.current_extent.unwrap_or(dimensions);
            let alpha = caps.supported_composite_alpha.iter().next().unwrap();
            let format = caps.supported_formats[0].0;

            Swapchain::new(device.clone(), surface.clone(),
            caps.min_image_count, format, dimensions, 1, caps.supported_usage_flags, &graphics_queue,
            SurfaceTransform::Identity, alpha, PresentMode::Fifo, true, None)
            .expect("failed to create swapchain")
        };

        let render_pass = Arc::new(vulkano::single_pass_renderpass!(device.clone(),
            attachments: {
                color: {
                    load: Clear,
                    store: Store,
                    format: swapchain.format(),
                    samples: 1,
                },
                depth: {
                    load: Clear,
                    store: DontCare,
                    format: Format::D16Unorm,
                    samples: 1,
                }
            },
            pass: {
                color: [color],
                depth_stencil: {depth}
            }
        ).unwrap());

        mod vs {
            vulkano_shaders::shader!{
                ty: "vertex",
                path: "src/shaders/shader.vert"
            }
        }

        mod fs {
            vulkano_shaders::shader!{
                ty: "fragment",
                path: "src/shaders/shader.frag"
            }
        }

        let vs = vs::Shader::load(device.clone()).expect("failed to create shader module");
        let fs = fs::Shader::load(device.clone()).expect("failed to create shader module");

        // TODO: Figure out if we need a separate pipeline object per material.
        // Find info/benchmarks about one complex shader vs multiple simple shaders.
        let graphics_pipeline = Arc::new(GraphicsPipeline::start()
            .vertex_input_single_buffer::<Vertex>()
            .cull_mode_back()
            //.polygon_mode_line() // wireframe mode
            //.polygon_mode_point() // vertex draw
            .vertex_shader(vs.main_entry_point(), ())
            .viewports_dynamic_scissors_irrelevant(1)
            .fragment_shader(fs.main_entry_point(), ())
            .depth_stencil_simple_depth()
            .render_pass(Subpass::from(render_pass.clone(), 0).unwrap())
            .build(device.clone())
            .unwrap());

        let recreate_swapchain = false;

        let mut dynamic_state = DynamicState::none();

        let (swapchain_fb, depth_buffer) = window_sized_buffers(
            &swap_images, 
            render_pass.clone(), 
            &mut dynamic_state, &device);

        let previous_frame_end = Some(Box::new(vulkano::sync::now(device.clone())) as Box<GpuFuture>);

        VulkanRender {
            surface,
            dimensions,
            //physical_device_index,
            device,
            graphics_queue,
            //present_queue, 
            render_pass,
            graphics_pipeline,
            swapchain,
            swapchain_fb,
            depth_buffer,
            vertex_buffers: vec![],
            index_buffers: vec![],
            camera: Point3::new(0.,0.,0.),
            uniform_buffers: vec![],
            descriptor_sets: vec![],
            previous_frame_end,
            recreate_swapchain,
            dynamic_state,
            rotation: (0.,0.),
            wire: false,
        }
    }

    // For now just assume there is one static object.
    pub fn setup_scene(&mut self, path: &Path) {
        match gltf::import(path) {
            Ok((document, buffers, images)) => {
                for mesh in document.meshes() {
                    for primitive in mesh.primitives() {
                        let reader = primitive.reader(|buffer| Some(&buffers[buffer.index()]));

                        let mut vert_pos = Vec::new();
                        if let Some(iter) = reader.read_positions() {
                            for vertex_position in iter {
                                vert_pos.push(vertex_position);
                            }
                        }
                        let mut normals = Vec::new();
                        if let Some(iter) = reader.read_normals() {
                            for normal in iter {
                                normals.push(normal);
                            }
                        }
                        let mut tex_coords = Vec::new();
                        if let Some(iter) = reader.read_tex_coords(0) {
                            for tex_coord in iter.into_f32() {
                                tex_coords.push(tex_coord);
                            }
                        }
                        let mut indices = Vec::new();
                        if let Some(iter) = reader.read_indices() {
                            for index in iter.into_u32() {
                                indices.push(index as u16);
                            }
                        }

                        // This is probably not optimal.
                        let vertices = vert_pos.iter().enumerate().map(|(idx, pos)| {
                            Vertex::new(*pos, normals[idx], tex_coords[idx])
                        }).collect();

                        self.add_buffer(vertices, indices);

                    }
                }

                // This is hacky and should use the actual camera object from gltf spec
                // or just use camera pos from player pos when we have one.
                match document.nodes().find(|node| node.name() == Some("Camera")) {
                    Some(node) => {
                        let camera = node.transform().decomposed();
                        let camera_point = Point3::new(camera.0[0], camera.0[1], camera.0[2]);
                        self.set_uniform_buffer(camera_point);
                    },
                    None => {
                        println!("No camera position found. Using default.");
                        let camera_point = Point3::new(0.0, -2.0, 1.0);
                        self.set_uniform_buffer(camera_point);
                    },
                }

                // Just dumping the image data to a different place.
                self.add_texture_images(&images);
                // TODO: Load the pbr material correctly instead.
                //let pbr = document.materials().next().unwrap().pbr_metallic_roughness();
                //let albedo_texture = pbr.base_color_texture().unwrap();
                        
            },
            Err(_) => {
                panic!("Model loading failed. Die.");
            },
        }
    }

    fn add_buffer(&mut self, vertices: Vec<Vertex>, indices: Vec<u16>) {
        let (vertex_buffer, future) = ImmutableBuffer::from_iter(
            vertices.iter().cloned(), 
            BufferUsage::vertex_buffer(), 
            self.graphics_queue.clone()).unwrap();

        future.flush().unwrap();
        self.vertex_buffers.push(vertex_buffer);

        let (index_buffer, future) = ImmutableBuffer::from_iter(
            indices.iter().cloned(), 
            BufferUsage::index_buffer(), 
            self.graphics_queue.clone()).unwrap();

        future.flush().unwrap();
        self.index_buffers.push(index_buffer);
    }

    fn set_uniform_buffer(&mut self, camera_point: Point3<f32>) {
        self.camera = camera_point;
        let (buffer, set) = create_uniform_buffer(
            &self.device, 
            &self.graphics_pipeline, 
            camera_point, 
            self.dimensions, 
            self.rotation);

        self.uniform_buffers.push(buffer);
        self.descriptor_sets.push(set);
    }

    fn add_texture_images(&mut self, images: &Vec<gltf::image::Data>) {
        let mut textures = vec![];
        let mut samplers = vec![];
        let mut futures = vec![];

        for image_data in images.iter() {
            let format = match image_data.format {
                gltf::image::Format::R8G8B8A8 => Format::R8G8B8A8Srgb,
                // TODO: Figure out what wiggling is needed for other formats,
                // as vulkano wont have three channel RGB data.
                // The gltf metallic-roughness is in Blue and Green of R8G8B8.
                // Maybe it's possible to swizzle into R8G8_UNORM.
                // The normal texture should be Snorm -1,1 to simplify shader.
                //gltf::image::Format::R8G8B8 => Format::R8G8B8Srgb,
                any => {
                    println!("this image format needs mapping to vulkan {:?}", any);
                    continue;
                },
            };

            let (texture, texture_future) = match ImmutableImage::from_iter(
                image_data.pixels.iter().cloned(), 
                Dimensions::Dim2d { width: image_data.width, height: image_data.height }, 
                format, 
                self.graphics_queue.clone()) {
                    Ok(tuple) => {
                                println!("good format: {:?}", format);
                                tuple
                                },
                    Err(err) => {
                        println!("Data length is likely wrong because of wrong format: {:?} {:?}", format, err);
                        continue;
                    },
                };


            // TODO: Read sampler setup from gltf.
            let sampler = Sampler::new(
                self.device.clone(), 
                Filter::Linear, 
                Filter::Linear, 
                MipmapMode::Linear, 
                SamplerAddressMode::Repeat,
                SamplerAddressMode::Repeat, 
                SamplerAddressMode::Repeat, 
                0.0, 
                1.0, 
                0.0, 
                std::f32::MAX).unwrap();

            textures.push(texture);
            samplers.push(sampler);
            futures.push(texture_future);
        }

        // Not sure if this is absolutely necessary, but since we share a queue with swapchain, we need to do this.
        let mut future: Box<dyn GpuFuture> = Box::new(futures.pop().unwrap());
        for fut in futures {
            future = Box::new(future.join(fut));
        }
        let future = future.then_signal_fence_and_flush();

        match future {
            Ok(_future) => {
                println!("material OK");
            },
            Err(err) => {
                println!("No future for the immutable images: {:?}", err);
            }
        }

        // TODO: Figure out if this can be a generic loop for any number of samplers.
        // TODO: The ordering should be deterministic and clearly defined,
        //       so the shader side is not guess work.
        if textures.len() == 2 { // Yeah not like this.
        let material_descriptor_set = PersistentDescriptorSet::start(
            self.graphics_pipeline.clone(), 1)
            .add_sampled_image(
                textures[0].clone(),
                samplers[0].clone())
            .unwrap()
            .add_sampled_image(
                textures[1].clone(),
                samplers[1].clone())
            .unwrap()
            .build().unwrap();
        self.descriptor_sets.push(Arc::new(material_descriptor_set));
        }

    }

    pub fn draw(&mut self) {
        self.previous_frame_end.as_mut().unwrap().cleanup_finished();

        if self.recreate_swapchain {
            let window = self.surface.window();
            let dimensions = if let Some(dimensions) = window.get_inner_size() {
                let dimensions: (u32, u32) = dimensions.to_physical(window.get_hidpi_factor()).into();
                [dimensions.0, dimensions.1]
            } else {
                return;
            };
            self.dimensions = dimensions;
            println!("New dimensions: {:?}", dimensions);

            let (new_swapchain, new_swap_images) = match self.swapchain.recreate_with_dimension(dimensions) {
                Ok(r) => r,
                Err(vulkano::swapchain::SwapchainCreationError::UnsupportedDimensions) => {
                    return;
                },
                Err(err) => panic!("{:?}", err)
            };

            self.swapchain = new_swapchain;
            let (swapchain_fb, depth_buffer) = window_sized_buffers(
                &new_swap_images, 
                self.render_pass.clone(), 
                &mut self.dynamic_state, 
                &self.device);
                
            self.swapchain_fb = swapchain_fb;
            self.depth_buffer = depth_buffer;
            
            let (buffer, set) = create_uniform_buffer(
                &self.device, 
                &self.graphics_pipeline, 
                self.camera, 
                self.dimensions, 
                self.rotation);

            self.uniform_buffers[0] = buffer;
            self.descriptor_sets[0] = set;

            self.recreate_swapchain = false;
        }

        let (image_num, acquire_future) = match vulkano::swapchain::acquire_next_image(self.swapchain.clone(), None) {
            Ok(r) => r,
            Err(vulkano::swapchain::AcquireError::OutOfDate) => {
                self.recreate_swapchain = true;
                return;
            },
            Err(err) => panic!("{:?}", err)
        };

        // TODO: Command buffer recording should be threaded if we have lot's of objects.
        let mut command_buffer = AutoCommandBufferBuilder::primary_one_time_submit(
            self.device.clone(),
            self.graphics_queue.family())
            .unwrap()
            .begin_render_pass(
                self.swapchain_fb[image_num].clone(),
                false,
                vec![[0.0, 0.0, 0.0, 1.0].into(), 1f32.into()])
            .unwrap();

        for (idx, vertex_buffer) in self.vertex_buffers.iter().enumerate() {
            // TODO: Use an atlassed vertex buffer and BufferSlice it.
            command_buffer = command_buffer.draw_indexed(
                self.graphics_pipeline.clone(),
                &self.dynamic_state,
                vec![vertex_buffer.clone()],
                self.index_buffers[idx].clone(),
                (self.descriptor_sets[0].clone(), self.descriptor_sets[1].clone()),
                ())
                .unwrap();
        }

        let command_buffer = command_buffer.end_render_pass()
            .unwrap()
            .build()
            .unwrap();

        // Actual render call is triggered here, submitting command buffer(s)
        let future = self.previous_frame_end.take().unwrap()
            .join(acquire_future)
            .then_execute(self.graphics_queue.clone(), command_buffer).unwrap()
            // TODO: Check if we can and should execute multiple command buffers.
            // E.g. multiple materials or cameras or something extra.
            .then_swapchain_present(self.graphics_queue.clone(), self.swapchain.clone(), image_num)
            .then_signal_fence_and_flush();

        match future {
            Ok(future) => {
                self.previous_frame_end = Some(Box::new(future) as Box<_>);
            },
            Err(vulkano::sync::FlushError::OutOfDate) => {
                self.recreate_swapchain = true;
                self.previous_frame_end = Some(Box::new(vulkano::sync::now(self.device.clone())) as Box<_>);
            },
            Err(err) => {
                println!("No future in vulkan: {:?}", err);
                self.previous_frame_end = Some(Box::new(vulkano::sync::now(self.device.clone())) as Box<_>);
            }
        }
    }

    pub fn rotate_view(&mut self, mouse_move: (f64, f64)) {
        let rotation_x = (self.rotation.0 + mouse_move.0 as f32 + 360.) % 360.;
        let rotation_y = (self.rotation.1 + mouse_move.1 as f32 + 360.) % 360.;
        self.rotation = (rotation_x, rotation_y);
        let (buffer, set) = create_uniform_buffer(
            &self.device, 
            &self.graphics_pipeline, 
            self.camera, 
            self.dimensions, 
            self.rotation);
        
        self.uniform_buffers[0] = buffer;
        self.descriptor_sets[0] = set;
    }

    pub fn recreate_swapchain(&mut self) {
        self.recreate_swapchain = true;
    }

    pub fn toggle_wireframe(&mut self) {
        self.wire = !self.wire;
        self.recreate_graphics_pipeline();
    }

    fn recreate_graphics_pipeline(&mut self) {
         mod vs {
            vulkano_shaders::shader!{
                ty: "vertex",
                path: "src/shaders/shader.vert"
            }
        }

        mod fs {
            vulkano_shaders::shader!{
                ty: "fragment",
                path: "src/shaders/shader.frag"
            }
        }

        let vs = vs::Shader::load(self.device.clone()).expect("failed to create shader module");
        let fs = fs::Shader::load(self.device.clone()).expect("failed to create shader module");

        // TODO: Figure out if we need a separate pipeline object per material.
        // Find info/benchmarks about one complex shader vs multiple simple shaders.
        let mut graphics_pipeline = GraphicsPipeline::start()
            .vertex_input_single_buffer::<Vertex>()
            .cull_mode_back();

        if self.wire {
            graphics_pipeline = graphics_pipeline.polygon_mode_line();
        } else {
            graphics_pipeline = graphics_pipeline.polygon_mode_fill();
        }
        let graphics_pipeline = graphics_pipeline
            .vertex_shader(vs.main_entry_point(), ())
            .viewports_dynamic_scissors_irrelevant(1)
            .fragment_shader(fs.main_entry_point(), ())
            .depth_stencil_simple_depth()
            .render_pass(Subpass::from(self.render_pass.clone(), 0).unwrap())
            .build(self.device.clone())
            .unwrap();
        
        self.graphics_pipeline = Arc::new(graphics_pipeline);
        self.recreate_swapchain = true;
    }
}

// Recreate the framebuffer stuff to window size.
fn window_sized_buffers(
    images: &[Arc<SwapchainImage<Window>>], 
    render_pass: Arc<dyn RenderPassAbstract + Send + Sync>, 
    dynamic_state: &mut DynamicState,
    device: &Arc<Device>
) -> (Vec<Arc<dyn FramebufferAbstract + Send + Sync>>, Arc<AttachmentImage>) {

    let dimensions = images[0].dimensions();

    let viewport = Viewport {
        origin: [0.0, 0.0],
        dimensions: [dimensions[0] as f32, dimensions[1] as f32],
        depth_range: 0.0 .. 1.0,
    };

    dynamic_state.viewports = Some(vec!(viewport));

    let attachment_usage = ImageUsage {
        transient_attachment: true,
        input_attachment: true,
        ..ImageUsage::none()
    };
    let depth_buffer = AttachmentImage::with_usage(
        device.clone(), 
        dimensions, 
        Format::D16Unorm, 
        attachment_usage)
        .unwrap();

    let swapchain_fb = images.iter().map(|image| {
        Arc::new(
            Framebuffer::start(render_pass.clone())
                .add(image.clone())
                .unwrap()
                .add(depth_buffer.clone())
                .unwrap()
                .build()
                .unwrap()
        ) as Arc<dyn FramebufferAbstract + Send + Sync>
    }).collect::<Vec<_>>();

    (swapchain_fb, depth_buffer)
}

fn create_uniform_buffer(
    device: &Arc<Device>, 
    pipeline: &Arc<dyn GraphicsPipelineAbstract + Send + Sync>, 
    camera_point: Point3<f32>, 
    dimensions: [u32;2], 
    rotation: (f32, f32)
) -> (Arc<CpuAccessibleBuffer<UniformBufferObject>>, Arc<dyn DescriptorSet + Send + Sync>) { 

    let y = Matrix4::from_angle_x(Rad::from(Deg(rotation.1)));
    let x = Matrix4::from_angle_z(Rad::from(Deg(rotation.0)));
    let model = x * y;
    let view = Matrix4::look_at(
        camera_point,
        Point3::new(0.0, 0.0, 0.0),
        Vector3::new(0.0, 0.0, 1.0)
    );
    let aspect = dimensions[0] as f32 / dimensions[1] as f32;
    let mut proj = cgmath::perspective(
        Rad::from(Deg(70.)), 
        aspect, 
        0.1, 
        100.0,
    );
    //println!("Aspect ratio: {:?}", aspect);

    proj.y.y *= -1.0;

    let ubo = UniformBufferObject { model, view, proj };
    let buffer = CpuAccessibleBuffer::from_data(
        device.clone(), 
        BufferUsage::uniform_buffer_transfer_destination(), 
        ubo)
        .unwrap();

    let set = Arc::new(PersistentDescriptorSet::start(pipeline.clone(), 0)
        .add_buffer(buffer.clone())
        .unwrap()
        .build()
        .unwrap());
    
    (buffer, set)
}
