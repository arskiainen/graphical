#version 450

layout(binding = 0) uniform UniformBufferObject {
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;

const vec3 light = vec3(-2.0,
                -2.0,
                3.0);

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 tex_coord;

layout(location = 0) out vec3 out_normal;
layout(location = 1) out vec2 out_tex_coord;
layout(location = 2) out vec3 out_light;
layout(location = 3) out vec3 out_eye;
//layout(location = 4) out vec3 out_color;


void main() {
    //vec3 light_dir = light-pos;
    //float light_dist = length(light_dir);
    //float dott = clamp(dot(normal, normalize(light_dir)), 0, 1) * 10.0 / (light_dist * light_dist);
    gl_Position = ubo.proj * ubo.view * ubo.model * vec4(pos, 1.0);
    out_normal = normalize(transpose(inverse(mat3(ubo.view * ubo.model))) * normalize(normal));
    out_eye = - vec3(ubo.view * vec4(pos, 1.0));
    out_light = vec3(ubo.view * ubo.model * vec4(light, 1.0)) + out_eye;
    out_tex_coord = tex_coord;
    //out_color = vec3(1.0, 1.0, 1.0) * dott;
}