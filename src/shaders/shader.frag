#version 450

layout(set = 1, binding = 0) uniform sampler2D normal;
layout(set = 1, binding = 1) uniform sampler2D albedo;

layout(location = 0) in vec3 n;
layout(location = 1) in vec2 texCoord;
layout(location = 2) in vec3 in_light;
layout(location = 3) in vec3 in_eye;
//layout(location = 4) in vec3 in_color;

layout(location = 0) out vec4 f_color;

void main() {
    float light_dist = length(in_light);
    vec3 both_n = (texture(normal, texCoord).rgb * 0.5 + 0.5) * n;
    float dott = clamp(dot(n, normalize(in_light)), 0, 1) * 10.0 / (light_dist * light_dist);

    vec3 e = normalize(in_eye);
    vec3 ref = reflect(-normalize(in_light), both_n);
    float spec = pow(clamp(dot(e,ref), 0, 1),5) * 10.0 / (light_dist * light_dist);

    f_color = texture(albedo, texCoord) * dott + vec4(1.0,1.0,1.0,1.0) * spec;
}